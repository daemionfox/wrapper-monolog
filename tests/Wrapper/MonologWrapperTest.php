<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 7/2/16
 * Time: 8:24 PM
 */

namespace Tests\Wrapper;


use org\bovigo\vfs\vfsStream;
use RCSI\Wrapper\ConfigWrapper;

class MonologWrapperTest extends \PHPUnit_Framework_TestCase
{

    protected $root;
    protected $config;

    public function setup()
    {
        $structure = array(
            'etc' => array(
                'phpunit-config' => array(
                        'config.ini' => '[LOGGING]
loggerName=logger-test
loggerPath=vfs://root/logs/test.log
loggerLevel=none
'
                )
            ),
            'logs' => array()
        );

        $this->root = vfsStream::setup('root');
        vfsStream::create($structure);
        ConfigWrapper::setPath("vfs://root/phpunit-config");
    }


}
