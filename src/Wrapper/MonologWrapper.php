<?php
/**
 * Created by PhpStorm.
 * User: msowers
 * Date: 6/10/15
 * Time: 9:17 AM
 */

namespace RCSI\Wrapper;

use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\ErrorLogHandler;
use RCSI\Monolog\Handler\MailHandler;

class MonologWrapper {
    /** Static instance of our logger
     * @var Logger
     */
    protected static $logger;

    public function addHandler(AbstractProcessingHandler $handler)
    {
        if (self::$logger === null) {
            self::init();
        }
        self::$logger->pushHandler($handler);
        return $this;
    }

    /** Initialize the logger and setup the stream handlers necessary for logging
     * @return Logger
     */
    public static function init()
    {
        if (self::$logger === null) {
            $me = new self();
            $config = ConfigWrapper::init();
            $logName = $config->get('loggerName', 'rcsi-monolog');  // Config after version 3.3.2 lets you define your default return
            $logPath = $config->get("loggerPath");
            $rotateLogs = $config->getBool('rotateLogs');
            $maxFiles = (int) $config->get('maxLogFiles', 0);
            $levels = $config->getArray('loggerLevel');
            $mailLevels = $config->getArray('loggerMailLevel');

            self::$logger = new Logger($logName);

            foreach ($levels as $logLevel) {
                // File logger
                if ($logLevel !== null && !empty($logPath)) {
                    $fileName = $logPath . "/" . strtolower($logLevel) . ".log";
                    $handler = $rotateLogs ? new RotatingFileHandler($fileName, $maxFiles, $logLevel) : new StreamHandler($fileName, $logLevel);
                    self::$logger->pushHandler($handler);
                }
                // Screen logger - only for debug
                if ($logLevel === Logger::DEBUG) {
                    self::$logger->pushHandler(new ErrorLogHandler());
                }
            }

            foreach ($mailLevels as $mailLog) {
                // Mail logs only if we specifically put it in the config file
                if ($mailLog !== null) {
                    self::$logger->pushHandler(
                        new MailHandler($mailLog)
                    );
                }
            }
        }
        return self::$logger;
    }

    protected function getLevel($level)
    {
        $logLevel = null;  // No logging for default
        switch (strtoupper($level)) {
            case "ALERT":
                $logLevel = Logger::ALERT;
                break;
            case "CRITICAL":
                $logLevel = Logger::CRITICAL;
                break;
            case "EMERGENCY":
                $logLevel = Logger::EMERGENCY;
                break;
            case "NOTICE":
                $logLevel = Logger::NOTICE;
                break;
            case "WARNING":
                $logLevel = Logger::WARNING;
                break;
            case "DEBUG":
                $logLevel = Logger::DEBUG;
                break;
            case "INFO":
                $logLevel = Logger::INFO;
                break;
            case "ERROR":
                $logLevel = Logger::ERROR;
                break;
        }
        return $logLevel;
    }

}
