<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 7/3/16
 * Time: 10:21 AM
 */

namespace RCSI\Monolog\Handler;


use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use RCSI\Wrapper\ConfigWrapper;
use Tx\Mailer\Message;
use Tx\Mailer\SMTP;

class MailHandler extends AbstractProcessingHandler
{
    protected $mailer;
    protected $config;

    public function __construct($level = Logger::ERROR, $bubble = true)
    {
        parent::__construct($level, $bubble);

        $this->config = ConfigWrapper::init();

        $mailServer = $this->config->get('mailServer');
        $mailPort   = $this->config->get('mailPort', 25);
        $mailUser   = $this->config->get('mailUser');
        $mailPass   = $this->config->get('mailPass');

        if (isset($mailServer)) {
            $this->mailer = new SMTP();
            $this->mailer
                ->setServer(
                    $mailServer,
                    $mailPort
                );
            if (isset($mailUser)) {
                $this->mailer
                    ->setAuth($mailUser, $mailPass);
            }
        }

    }


    /**
     * Writes the record down to the log of the implementing handler
     *
     * @param  array $record
     * @return void
     */
    protected function write(array $record)
    {
        if (isset($this->mailer)) {
            $msg      = $record['message'];
            $level    = $record['level_name'];
            $channel  = $record['channel'];
            $datetime = $record['datetime'];
            $msgDate  = $datetime->format('Y-m-d H:i:s e');
            $body     = "<p><b>A {$level} error was detected by {$channel}</b></p>\n<p>Date: {$msgDate}</p>\n<p>Message: {$msg}</p>\n";
            $subject = $this->config->get('loggerMailSubject', "Logger: " . Logger::getLevelName($this->level) . " found, details in message");
            $toList  = $this->config->getArray('loggerMailTo');
            $from    = $this->config->getArray('loggerMailFrom', 'no-reply@nothing.com');

            $message = new Message();
            $message
                ->setFrom('Logger', $from)
                ->setSubject($subject)
                ->setBody($body);

            foreach ($toList as $tl) {
                $message->setTo($tl, $tl);
            }
            $this->mailer->send($message);
        }
    }
}
